﻿/**************************************************************
 * Tym: Project not found
 *
 * Autori:    Filip Jahn    (xjahnf00)
 *            Zdenek Dobes  (xdobes21)
 *            Jan Kepert    (xkeper00)
 *            Michal Luner  (xluner01)
 *
 * Datum:     4/2020
 *
 * Part:      Testy matematickych funkci kalkulacky
 **************************************************************/

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Calculator.Tests
{
    [TestClass()]
    public class Form1Tests
    {
        Operace form = new Operace();

        //--------- KLIK na tlacitko ---------
        [TestMethod()]
        public void button_click()
        {
            //Kontrola stisknuti tlacitka
            //  1 a 9 a ,
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button9, null);
            Assert.AreEqual("1,9", form.resultTextBox.Text);

            //  0 a 8
            form.button_click(form.button0, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button8, null);
            Assert.AreEqual("1,908", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //  2 a7
            form.button_click(form.button2, null);
            form.button_click(form.button7, null);
            Assert.AreEqual("27", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // 6 a 5
            form.button_click(form.button6, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button5, null);
            Assert.AreEqual("6,5", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // 3 a 4
            form.button_click(form.button3, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button4, null);
            Assert.AreEqual("3,4", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- KLIK na CE ---------
        [TestMethod()]
        public void klik_na_CE()
        {
            form.button_click(form.button3, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button4, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            form.buttonCE_Click(form.buttonCE, null);
            Assert.AreEqual(" 3,4 =", form.labelSubtotal.Text);
            Assert.AreEqual("0", form.resultTextBox.Text);
        }

        //--------- KLIK na C ---------
        [TestMethod()]
        public void klik_na_C()
        {
            form.button_click(form.button4, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button6, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            form.buttonClear_Click(form.buttonClear, null);
            Assert.AreEqual("", form.labelSubtotal.Text);
            Assert.AreEqual("0", form.resultTextBox.Text);
        }

        //--------- KLIK na konvektor +/- ---------
        [TestMethod()]
        public void convertorOperator()
        {
            //  Prevod na zaporne cisla
            form.button_click(form.button8, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            Assert.AreEqual("-8", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //  Prevod na kladne cislo
            form.button_click(form.button8, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            Assert.AreEqual("8", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- KLIK na operator ---------
        [TestMethod()]
        public void operator_click()
        {
            form.button_click(form.button1, null);
            form.operator_click(form.buttonMinus, null);
            Assert.AreEqual("1 -", form.labelSubtotal.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            form.button_click(form.button1, null);
            form.operator_click(form.buttonPlus, null);
            Assert.AreEqual("1 +", form.labelSubtotal.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            form.button_click(form.button1, null);
            form.operator_click(form.buttonMultiplication, null);
            Assert.AreEqual("1 ×", form.labelSubtotal.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            form.button_click(form.button1, null);
            form.operator_click(form.buttonDivision, null);
            Assert.AreEqual("1 ÷", form.labelSubtotal.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- SCITANI kladna cisla ---------
        [TestMethod()]
        public void secti_kladna_cisla()
        {
            //Scitani 1,8 + 8
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button8, null);
            form.operator_click(form.buttonPlus, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("9,8", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Scitani zaporneho cisla s kladnym s pouzitim convertoru znamenka.
            form.button_click(form.button1, null);      //but 1
            form.button_click(form.buttonPoint, null);  //but ,
            form.button_click(form.button8, null);      //but 8     = 1,8
            form.buttonConvertor_Click(form.buttonConvertor, null); // = -1,8
            form.operator_click(form.buttonPlus, null);   //but +
            form.button_click(form.button8, null);    //but 8
            form.buttonEquals_Click(form.buttonEquals, null);   // -1,8 + 8 = 6,2
            Assert.AreEqual("6,2", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- SCITANI zaporna cisla ---------
        [TestMethod()]
        public void secti_zaporna_cisla()
        {
            //Scitani -1,8 + 8
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button8, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonPlus, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
             Assert.AreEqual("6,2", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
            
            ////Scitani zaporneho cisla s kladnym s pouzitim convertoru znamenka.
            //form.button_click(form.button1, null);      //but 1
            //form.button_click(form.buttonPoint, null);  //but ,
            //form.button_click(form.button8, null);      //but 8     = 1,8
            //form.buttonConvertor_Click(form.buttonConvertor, null); // = -1,8
            //form.operator_click(form.buttonPlus, null);   //but +
            //form.button_click(form.button8, null);    //but 8
            //form.buttonConvertor_Click(form.buttonConvertor, null); // -1,8 + (-8)
            //form.buttonEquals_Click(form.buttonEquals, null);
            //Assert.AreEqual("-9,8", form.resultTextBox.Text);
            ////Vycisteni
            //form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- ODCITANI kladneho cisla ---------
        [TestMethod()]
        public void odcitani_kladna_cisla()
        {
            //  8 - 1,6
            form.button_click(form.button8, null);
            form.operator_click(form.buttonMinus, null);
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button6, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("6,4", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //  1,8 - 4
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button8, null);
            form.operator_click(form.buttonPlus, null); //Zkouska jestli se spravne prehodi znamenka
            form.operator_click(form.buttonMinus, null);
            form.button_click(form.button4, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-2,2", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Odcitani stejne velkych cisel
            form.button_click(form.button8, null);
            form.operator_click(form.buttonMinus, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.IsTrue(form.resultTextBox.Text == "0");
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- ODCITANI zaporna cisla ---------
        [TestMethod()]
        public void odcitani_zaporna_cisla()
        {
            //  -8 - 1,8
            form.button_click(form.button8, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonMinus, null);
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-9,8", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //  -1,5 - 3
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button5, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonPlus, null); //Zkouska jestli se spravne prehodi znamenka
            form.operator_click(form.buttonMinus, null);
            form.button_click(form.button3, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-4,5", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Odcitani zaporneho cisla od kladneho
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button8, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonMinus, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-9,8", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //  -8 - 8
            form.button_click(form.button8, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonMinus, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.IsTrue(form.resultTextBox.Text == "-16");
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- NASOBENI kladneho cisla ---------
        [TestMethod()]
        public void nasob_kladna_cisla()
        {
            //Dve cisla krat jedno
            form.button_click(form.button2, null);
            form.button_click(form.button4, null);
            form.operator_click(form.buttonMultiplication, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("192", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Jedno cislo krat dve
            form.button_click(form.button4, null);
            form.operator_click(form.buttonMultiplication, null);
            form.button_click(form.button8, null);
            form.button_click(form.button2, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("328", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Nasobeni vetsich cisel
            for (int i = 0; i < 3; i++)
                form.button_click(form.button2, null);
            form.operator_click(form.buttonMultiplication, null);
            for (int i = 0; i < 8; i++)
                form.button_click(form.button3, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("7399999926", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
            
            //Nasobeni malych cisel
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button7, null);
            form.button_click(form.button6, null);
            form.button_click(form.button3, null);
            form.operator_click(form.buttonMultiplication, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button2, null);
            form.button_click(form.button1, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("0,16023", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- NASOBENI zaporneho cisla ---------
        [TestMethod()]
        public void nasob_zaporna_cisla()
        {
            // -24 * 8
            form.button_click(form.button2, null);
            form.button_click(form.button4, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonMultiplication, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-192", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Nasobeni vetsich zapornych cisel
            for (int i = 0; i < 3; i++)
                form.button_click(form.button2, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonMultiplication, null);
            for (int i = 0; i < 8; i++)
                form.button_click(form.button3, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-7399999926", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Nasobeni zaporneho cisla
            form.button_click(form.button7, null);
            form.button_click(form.button6, null);
            form.button_click(form.button3, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonMultiplication, null);
            form.button_click(form.button2, null);
            form.button_click(form.button1, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-16023", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Nasobeni malych cisel
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button7, null);
            form.button_click(form.button6, null);
            form.button_click(form.button3, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonMultiplication, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button2, null);
            form.button_click(form.button1, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-0,16023", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }


        //--------- DELENI kladna cisla ---------
        [TestMethod()]
        public void del_kladna_cisla()
        {
            //Dve cisla deleno jednim
            form.button_click(form.button2, null);
            form.button_click(form.button4, null);
            form.operator_click(form.buttonDivision, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("3", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Deleni vetsich cisel
            form.buttonClear_Click(form.buttonClear, null);
            for (int i = 1; i <= 8; i++)
                form.button_click(form.button8, null);
            form.operator_click(form.buttonDivision, null);
            form.button_click(form.button1, null);
            form.button_click(form.button0, null);
            form.button_click(form.button0, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            form.operator_click(form.buttonDivision, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("111111,11", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Deleni malych cisel
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button7, null);
            form.button_click(form.button6, null);
            form.button_click(form.button3, null);
            form.operator_click(form.buttonMultiplication, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button2, null);
            form.button_click(form.button1, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("0,16023", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- DELENI zaporneho cisla ---------
        [TestMethod()]
        public void del_zaporna_cisla()
        {
            //  -24/8
            form.button_click(form.button2, null);
            form.button_click(form.button4, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonDivision, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-3", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Deleni vetsich cisel
            form.buttonClear_Click(form.buttonClear, null);
            for (int i = 1; i <= 8; i++)
                form.button_click(form.button8, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonDivision, null);
            form.button_click(form.button1, null);
            form.button_click(form.button0, null);
            form.button_click(form.button0, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            form.operator_click(form.buttonDivision, null);
            form.button_click(form.button8, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-111111,11", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            //Deleni malych cisel
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button7, null);
            form.button_click(form.button6, null);
            form.button_click(form.button3, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonMultiplication, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button2, null);
            form.button_click(form.button1, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("-0,16023", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- ODMOCNINA kladneho cisla ---------
        [TestMethod()]
        public void odm_kladna_cisla()
        {
            // Sqrt z 25 = 5
            form.operator_click(form.buttonNotTo, null);
            form.button_click(form.button2, null);
            form.button_click(form.button5, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("5", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // Treti odmocnina ze 125 = 5
            form.button_click(form.button3, null);
            form.operator_click(form.buttonNotTo, null);
            form.button_click(form.button1, null);
            form.button_click(form.button2, null);
            form.button_click(form.button5, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("5", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // Pata odmocnina ze 1,5 = 1,08447177
            form.button_click(form.button5, null);
            form.operator_click(form.buttonNotTo, null);
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button5, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("1,0844717711977", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // 1,5ta odmocnina ze 25 = 1,08447177
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button5, null);
            form.operator_click(form.buttonNotTo, null);
            form.button_click(form.button2, null);
            form.button_click(form.button5, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.IsTrue(25 > Double.Parse(form.resultTextBox.Text));
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- ODMOCNINA zaporneho cisla ---------
        [TestMethod()]
        public void odm_zaporna_cisla()
        {
            // Sqrt z -25 = NaN
            form.operator_click(form.buttonNotTo, null);
            form.button_click(form.button2, null);
            form.button_click(form.button5, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("NaN", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // Minus treti odmocnina ze 125 = 0,2
            form.button_click(form.button3, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonNotTo, null);
            form.button_click(form.button1, null);
            form.button_click(form.button2, null);
            form.button_click(form.button5, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("0,2", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // Minus druha odmocnina ze 25 = 0,04
            form.button_click(form.button2, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonNotTo, null);
            form.button_click(form.button2, null);
            form.button_click(form.button5, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("0,2", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // 1,5ta odmocnina ze 25 = 0,116960709528515
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button5, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.operator_click(form.buttonNotTo, null);
            form.button_click(form.button2, null);
            form.button_click(form.button5, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("0,116960709528515", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- ZAOKROUHLI kladna cisla ---------
        [TestMethod()]
        public void zaokrouhli_kladna_cisla()
        {
            // 81, 5 round = 82
            form.button_click(form.button8, null);
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button5, null);
            form.buttonRound_Click(form.buttonRound, null);
            Assert.AreEqual("82", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // 0,125 round = 0
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button1, null);
            form.button_click(form.button2, null);
            form.button_click(form.button5, null);
            form.buttonRound_Click(form.buttonRound, null);
            Assert.AreEqual("0", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // 2,4999 round = 2
            form.button_click(form.button2, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button4, null);
            form.button_click(form.button9, null);
            form.button_click(form.button9, null);
            form.button_click(form.button9, null);
            form.button_click(form.button9, null);
            form.buttonRound_Click(form.buttonRound, null);
            Assert.AreEqual("2", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }

        //--------- ZAOKROUHLI zaporna cisla ---------
        [TestMethod()]
        public void zaokrouhli_zaporna_cisla()
        {
            // -81,5 round = -82
            form.button_click(form.button8, null);
            form.button_click(form.button1, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button5, null);
            form.buttonRound_Click(form.buttonRound, null);
            Assert.AreEqual("-82", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // -0,125 round = 0
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button1, null);
            form.button_click(form.button2, null);
            form.button_click(form.button5, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.buttonRound_Click(form.buttonRound, null);
            Assert.AreEqual("0", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);

            // -2,4999 round = -2
            form.button_click(form.button2, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button4, null);
            form.button_click(form.button9, null);
            form.button_click(form.button9, null);
            form.button_click(form.button9, null);
            form.button_click(form.button9, null);
            form.buttonConvertor_Click(form.buttonConvertor, null);
            form.buttonRound_Click(form.buttonRound, null);
            Assert.AreEqual("-2", form.resultTextBox.Text);
            //Vycisteni
            form.buttonClear_Click(form.buttonClear, null);
        }




        //------------------- ZDENDA -------------------


        [TestMethod()]
        public void factorial()
        {
            //Faktorial 0
            form.button_click(form.button0, null);
            form.operator_click(form.buttonFactorial, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("1", form.resultTextBox.Text);

            //Faktorial 1
            form.button_click(form.button1, null);
            form.operator_click(form.buttonFactorial, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("1", form.resultTextBox.Text);

            //Faktorial desetinny
            form.button_click(form.button1, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button1, null);
            form.operator_click(form.buttonFactorial, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("0,11", form.resultTextBox.Text);

            //Faktorial vetsich cisel
            form.button_click(form.button1, null);
            form.button_click(form.button5, null);
            form.operator_click(form.buttonFactorial, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("1307674368000", form.resultTextBox.Text);
        }
        [TestMethod()]
        public void backbutton()
        {
            //Backbutton malych cisel
            form.button_click(form.button7, null);
            form.buttonBack_Click(form.buttonBack, null);
            Assert.AreEqual("0", form.resultTextBox.Text);
            form.operator_click(form.buttonClear, null);

            //Backbutton desetinnych cisel
            form.button_click(form.button4, null);
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button2, null);
            form.button_click(form.button8, null);
            form.buttonBack_Click(form.buttonBack, null);
            Assert.AreEqual("4,2", form.resultTextBox.Text);
            form.operator_click(form.buttonClear, null);

            //Backbutton vicekrat v rade
            for (int i = 0; i < 4; i++)
            {
                form.button_click(form.button4, null);
                form.button_click(form.button2, null);
            }
            for (int i = 0; i < 6; i++)
            {
                form.buttonBack_Click(form.buttonBack, null);
            }
            Assert.AreEqual("42", form.resultTextBox.Text);
            form.operator_click(form.buttonClear, null);

            //Backbutton nuly
            form.button_click(form.button0, null);
            form.buttonBack_Click(form.buttonBack, null);
            Assert.AreEqual("0", form.resultTextBox.Text);
            form.operator_click(form.buttonClear, null);

            //Backbutton zaporneho cisla
            form.button_click(form.button7, null);
            form.button_click(form.button8, null);
            form.operator_click(form.buttonConvertor, null);
            form.buttonBack_Click(form.buttonBack, null);
            form.buttonBack_Click(form.buttonBack, null);
            Assert.AreEqual("0", form.resultTextBox.Text);
            form.operator_click(form.buttonClear, null);
        }
        [TestMethod()]
        public void squaredNumbers()
        {
            //Mocnina pro cela mala cisla
            form.button_click(form.button4, null);
            form.operator_click(form.buttonTo, null);
            form.button_click(form.button2, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("16", form.resultTextBox.Text);

            //Mocnina pro velka cela cisla
            for (int i = 0; i < 5; i++)
            {
                form.button_click(form.button9, null);
            }
            form.operator_click(form.buttonTo, null);
            form.button_click(form.button2, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("9999800001", form.resultTextBox.Text);

            //Mocnina pro velke expomenty
            form.button_click(form.button1, null);
            form.button_click(form.button0, null);
            form.operator_click(form.buttonTo, null);
            form.button_click(form.button9, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("1000000000", form.resultTextBox.Text);

            //Mocnina pro mala desetinna
            form.button_click(form.buttonPoint, null);
            form.button_click(form.button4, null);
            form.button_click(form.button2, null);
            form.operator_click(form.buttonTo, null);
            form.button_click(form.button2, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("0,1764", form.resultTextBox.Text);

            //Mocnina na nultou
            form.button_click(form.button0, null);
            form.operator_click(form.buttonTo, null);
            form.button_click(form.button0, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            Assert.AreEqual("1", form.resultTextBox.Text);
        }
    }
}