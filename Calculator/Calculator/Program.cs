﻿/**************************************************************
 * Tym: Project not found
 *
 * Autori:    Filip Jahn    (xjahnf00)
 *            Zdenek Dobes  (xdobes21)
 *            Jan Kepert    (xkeper00)
 *            Michal Luner  (xluner01)
 *
 * Datum:     4/2020
 *
 * Part:      Hlavni trida kalkulacky
 **************************************************************/

using System;
using System.Windows.Forms;

namespace Calculator
{
    public class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Operace());
        }
    }
}
