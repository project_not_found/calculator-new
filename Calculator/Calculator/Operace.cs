﻿/**************************************************************
 * Tym: Project not found
 *
 * Autori:    Filip Jahn    (xjahnf00)
 *            Zdenek Dobes  (xdobes21)
 *            Jan Kepert    (xkeper00)
 *            Michal Luner  (xluner01)
 *
 * Datum:     4/2020
 *
 * Part:      Matematicka knihovna kalkulacky
 **************************************************************/

using System;
using System.Linq;
using System.Windows.Forms;

/// <summary>
///     Hlavni funkce kalkulacky. Veskere operace, ktere kalkulacka nabizi.
/// </summary>
namespace Calculator
{
    public partial class Operace : Form
    {
        Double resultValue;                 // Slouzi pro uchovavani mezivypoctu.
        String operatorCharacter = "";      // Slouzi k zapamatovani si posledni operace, kterou chceme provest.
        bool operatorIsClicked = false;    
        bool equalButtonClicked = false;

        public Operace()
        {
            InitializeComponent();
        }

        /// <summary>
		///     Funkce slouzi pro nacteni cisla do resultTextBox, na ktere se kliklo, popr. desetinne 'carky'.
		/// </summary>
		/// <returns>Funkce vraci cislo a vypisuje jej do casti resultTextBox, ktera se nachazi na kalkulacce nad cisly a operacemi.</returns>
        public void button_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            
            if ((resultTextBox.Text == "0") || (operatorIsClicked))
                resultTextBox.Clear();
            else if (equalButtonClicked)
            {
                labelSubtotal.Text = "";
                resultTextBox.Text = "";
                equalButtonClicked = false;
            }
            if (button.Text == ",")
            {
                if (!resultTextBox.Text.Contains(","))
                    resultTextBox.Text = resultTextBox.Text + button.Text;
            }
            else
                resultTextBox.Text = resultTextBox.Text + button.Text;
            equalButtonClicked = false;
            operatorIsClicked = false;
        }

        /// <summary>
		///     Funkce se vola, pokud jsme stiskli nejakou matematickou operaci a provede mezivypocet.
		/// </summary>
		/// <returns>Vraci mezivypocet, ktery muzeme videt v labelSubtotal. To je umisteno nad hlavnim vypisem.</returns>
        public void operator_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            if (resultValue != 0)
                buttonEquals.PerformClick();
            //Pokud stiskneme odmocninu a neudame, ktera se ma provest, provede se druha odmocnina.
            if (button.Text == "√" && resultTextBox.Text == "0")
                resultValue = 2;
            else
                resultValue = Double.Parse(resultTextBox.Text);
            operatorCharacter = button.Text;
            labelSubtotal.Text = resultValue + " " + operatorCharacter;
            operatorIsClicked = true;
        }

        /// <summary>
		///     Funkce pro vycisteni zaznamu na hlavnim displeji.
		/// </summary>
		/// <returns>Funkce vlozi na displej hodnotu nula.</returns>
        public void buttonCE_Click(object sender, EventArgs e)
        {
            resultTextBox.Text = "0";
        }

        /// <summary>
		///     Funkce cisti mezipamet, hlavni displej a posledni nacteny operator.
		/// </summary>
		/// <returns>Funkce vlozi na displej hodnotu nula a vynuluje veskere promenne.</returns>
        public void buttonClear_Click(object sender, EventArgs e)
        {
            resultTextBox.Text = "0";
            resultValue = 0;
            operatorCharacter = "";
            labelSubtotal.Text = "";
        }

        /// <summary>
		///     Tato funkce se vola, pokud jsme stiskli tlacitko rovna se nebo jakykoliv operator.
        ///     Jednotlive operace jsou popsany u kazde operace zvlast.
		/// </summary>
		/// <returns>
        ///     Funkce vzdy vraci do mezivypoctoveho displeje - labelSubtotal resultValue a znamenko operatoru. 
        ///     Do resultTextBox ukladame pak vysledek po provedeni operace, ktera se rozhoduje switchem.
        /// </returns>
        public void buttonEquals_Click(object sender, EventArgs e)
        {
            equalButtonClicked = true;
            if (labelSubtotal.Text == null || labelSubtotal.Text.Contains('='))
            {
            }
            else
                {
                // Vypis mezivypoctu plus znamenka, dale cisla, se kterym chceme provest operaci a tlacitka rovna se.
                labelSubtotal.Text = labelSubtotal.Text + " " + resultTextBox.Text + " " + "=";
                switch (operatorCharacter)
                {
                    // Scitani cisla ulozeneho v promenne resultValue a zadaneho cisla.
                    case "+":
                        resultTextBox.Text = (resultValue + Double.Parse(resultTextBox.Text)).ToString();
                        break;

                    // Odcitani cisla ulozeneho v promenne resultValue a zadaneho cisla.
                    case "-":
                        resultTextBox.Text = (resultValue - Double.Parse(resultTextBox.Text)).ToString();
                        break;

                    // Nasobeni cisla ulozeneho v promenne resultValue a zadaneho cisla.
                    case "×":
                        resultTextBox.Text = (resultValue * Double.Parse(resultTextBox.Text)).ToString();
                        break;

                    // Deleni cisla ulozeneho v promenne resultValue a zadaneho cisla.
                    case "÷":
                        resultTextBox.Text = (resultValue / Double.Parse(resultTextBox.Text)).ToString();
                        break;

                    // Pri faktorialu nuly vracime jednicku.
                    // Jinak v cyklu nasobime od zadaneho cisla vzdy cislo o jedna mensi.
                    case "!":
                        labelSubtotal.Text = resultValue + "! =";
                        if (resultValue == 0)
                        {
                            resultTextBox.Text = "1";
                        }
                        else
                        {
                            Double number = resultValue - 1;
                            while (number > 0)
                            {
                                resultValue = resultValue * number;
                                number--;
                            }
                            resultTextBox.Text = resultValue.ToString();
                        }
                        break;

                    // Na mocninu pouzivame knihovni funkci Pow z knihovny Math.
                    /// <param name="resultValue">Nactene cislo, ktere se ma xkrat nasobit.</param>
                    /// <param name="mocnina">Mocnina, na kterou ma byt cislo umocneno.</param>
                    case "xⁿ":
                        Double mocnina = Double.Parse(resultTextBox.Text);
                        resultValue = Math.Pow(resultValue, mocnina);
                        resultTextBox.Text = resultValue.ToString();
                        break;

                    // Na odmocninu pouzivame knihovni funkci Pow z knihovny Math.
                    case "√":
                        Double cislo = Double.Parse(resultTextBox.Text);    //cislo, ktere chceme odmocnit
                        Double exponent = resultValue;                      //exponent = kolikatou odmocninu provest
                        if (exponent == 0)
                        {
                            exponent = 2;
                        }
                        exponent = 1 / exponent;
                        resultValue = Math.Pow(cislo, exponent);
                        resultTextBox.Text = resultValue.ToString();
                        break;
                }
            }
        }


        /// <summary>
		///     Po stisknuti tlacitka Back se odmaze vzdy posledni znak z displeje.
		/// </summary>
		/// <returns>Odmaze posledni znak.</returns>
        public void buttonBack_Click(object sender, EventArgs e)
        {
            char lastCharacter = resultTextBox.Text.Last();
            int lastPosition = resultTextBox.Text.LastIndexOf(lastCharacter);
            if(resultTextBox.Text.First() == '-' && lastPosition == 1)
            {
                resultTextBox.Text = "0";
            }
            else if (lastPosition != 0)
            {
                resultTextBox.Text = resultTextBox.Text.Remove(lastPosition, 1);
            }
            else
            {
                resultTextBox.Text = "0";
            }
        }

        /// <summary>
		///     Po stisknuti tlacitka Convert se zmeni znamenko vysledku.
		/// </summary>
		/// <returns>Zmeni se z + na - a opacne.</returns>
        public void buttonConvertor_Click(object sender, EventArgs e)
        {
            resultValue = Double.Parse(resultTextBox.Text);
            resultValue = -resultValue;
            resultTextBox.Text = resultValue.ToString();
        }

        /// <summary>
		///     Po stisknuti tlacitka se zaokrouhli cislo na hlavnim displeji.
		/// </summary>
		/// <returns>Cislo na hlavnim displeji se zaokrouhli na cele cislo.</returns>
        public void buttonRound_Click(object sender, EventArgs e)
        {
            resultValue = Double.Parse(resultTextBox.Text);
            if (string.Compare(resultValue.ToString(), ",") != 0)
                resultValue = Math.Round(resultValue);
            resultTextBox.Text = resultValue.ToString();
        }
    }
}