﻿/**************************************************************
 * Tym: Project not found
 *
 * Autori:    Filip Jahn    (xjahnf00)
 *            Zdenek Dobes  (xdobes21)
 *            Jan Kepert    (xkeper00)
 *            Michal Luner  (xluner01)
 *
 * Datum:     4/2020
 *
 * Part:      Profiling za pouziti mat. funkci z nasi kalkulacky
 **************************************************************/

using System;
using System.Diagnostics;

namespace Calculator.Profiling
{
    public class Profiling
    {
        // Abychom mohli pouzivat funkce knihovny
        static Operace form = new Operace();

        static int Main(string[] args)
        {
            double result = prof();
            Console.WriteLine(result);
            return 0;
        }
        static double prof()
        {
            Stopwatch process_time = new Stopwatch();

            //vstupni promenne
            int[] input = new int[20];  //cislo ze vstupu
            int counter = 0;            //pocet cislic cisla
            int number = 0;             //cislo ze vstupu po konkatenaci
            double[] cisla = new double[1024];  //vsechna cisla

            double sum = 0;             //suma cisel
            double x_s_carkou = 0;      //x s carkou
            int N = 0;                  //pocet cisel

            process_time.Start();       //zapiname casovac

            // vstup
            do // -1 return, kdyz nema co nacitat
            {
                //cte 1 cislo po cislicich do pole
                counter = 0;
                do
                {
                    input[counter] = Console.Read();
                    counter++;
                } while (input[counter - 1] != ' ' && input[counter - 1] != '\n' && input[counter - 1] != -1);
                //kontrola prebytecnych znaku a podle nich meni pocet cislic 
                for (int i = counter - 1; (input[i] < 48 || input[i] > 57) && input[i] != -1; i--)
                {
                    counter--;
                    if (counter == 0)
                    {
                        input[0] = ' ';
                        break;
                    }
                }
                //specialni debug kvuli konci
                if ((counter > 1 && input[counter - 1] == -1) || (counter == 1 && input[counter - 1] == -1))
                {
                    counter--;
                }
                //konkatenace
                if (counter > 1 && N < 1024)
                {
                    number = concatenate(input, counter);
                    sum += number;
                    cisla[N] = number;
                    N++;
                }
                else if (input[0] != ' ' && input[0] != '\n' && input[0] != -1 && N < 1024)
                {
                    number = input[0] - 48; //ASCII konstanta
                    sum += number;
                    cisla[N] = number;
                    N++;
                }
            } while (input[counter] != -1);



            //profiling
            //x_s_carkou = sum / N;
            form.resultTextBox.Text = sum.ToString();
            deleni();
            form.resultTextBox.Text = N.ToString();
            form.buttonEquals_Click(form.buttonEquals, null);
            x_s_carkou = Double.Parse(form.resultTextBox.Text);

            //double result = Math.Sqrt((1 / N - 1) * ((x_s_carkou * x_s_carkou) - (N * x_s_carkou * x_s_carkou)));

            //prochazime pole cisel, kazde cislo minus aritmeticky prumer vsech cisel
            for (int i = 0; i < N; i++)
            {
                form.resultTextBox.Text = cisla[i].ToString();
                odcitani();
                form.resultTextBox.Text = x_s_carkou.ToString();
                form.buttonEquals_Click(form.buttonEquals, null);
                cisla[i] = Double.Parse(form.resultTextBox.Text);
                form.buttonClear_Click(form.buttonClear, null);

                //kazde cislo na druhou
                form.resultTextBox.Text = cisla[i].ToString();
                mocnina();
                form.button_click(form.button2, null);
                form.buttonEquals_Click(form.buttonEquals, null);
                cisla[i] = Double.Parse(form.resultTextBox.Text);
                form.buttonClear_Click(form.buttonClear, null);
            }

            //promenna pro soucet vsech cisel
            double sum_of_diferrences = 0;
            //postupne scitani vsech cisel
            for (int j = 0; j < N; j++)
            {
                form.resultTextBox.Text = sum_of_diferrences.ToString();
                scitani();
                form.resultTextBox.Text = cisla[j].ToString();
                form.buttonEquals_Click(form.buttonEquals, null);
                sum_of_diferrences = Double.Parse(form.resultTextBox.Text);
                form.buttonClear_Click(form.buttonClear, null);
            }

            //pocet cisel minus jedna
            double n_minus_one;
            form.resultTextBox.Text = N.ToString();
            odcitani();
            form.button_click(form.button1, null);
            form.buttonEquals_Click(form.buttonEquals, null);
            n_minus_one = Double.Parse(form.resultTextBox.Text);

            //vydeleni sumou cisel promennou n_minus_one
            form.resultTextBox.Text = sum_of_diferrences.ToString();
            deleni();
            form.resultTextBox.Text = n_minus_one.ToString();
            form.buttonEquals_Click(form.buttonEquals, null);
            sum_of_diferrences = Double.Parse(form.resultTextBox.Text);
            form.buttonClear_Click(form.buttonClear, null);


            //vysledna odmocnina 
            form.button_click(form.button2,null);
            odmocnina();
            form.resultTextBox.Text = sum_of_diferrences.ToString();
            form.buttonEquals_Click(form.buttonEquals, null);
            double result = Double.Parse(form.resultTextBox.Text);
            form.buttonClear_Click(form.buttonClear, null);

            //vracime vysledek pro vypsani do konzole
            return result;
        }
        //konkatenace z pole cislic na cislo
        static int concatenate(int[] input, int counter)
        {
            int number = input[counter - 1] - 48;
            if (counter > 1)
            {
                number = int.Parse(((concatenate(input, counter - 1).ToString()) + ((input[counter - 1] - 48).ToString())));
            }
            return number;
        }

        static void deleni()
        {
            form.operator_click(form.buttonDivision, null);
        }

        static void odcitani()
        {
            form.operator_click(form.buttonMinus, null);
        }

        static void mocnina()
        {
            form.operator_click(form.buttonTo, null);
        }

        static void scitani()
        {
            form.operator_click(form.buttonPlus, null);
        }

        static void odmocnina()
        {
            form.operator_click(form.buttonNotTo, null);
        }
    }
}